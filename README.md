## CRMI API

This project implemements a RESTful API for the Atom-based [[RFC4287]](https://datatracker.ietf.org/doc/html/rfc4287) Syndication Feed profiled in the Distribution section of the FHIR Canonical Resources Management IG [[CRMI](https://hl7.org/fhir/uv/crmi/distribution.html#distribution-syndication)].

The API is implemented using the [FastAPI](https://fastapi.tiangolo.com/) framework. Write requests require authentication using random key that is generated and written to disk on the first application launch.