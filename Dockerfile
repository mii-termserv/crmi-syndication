FROM python:3.12-alpine AS python-base
WORKDIR /app
RUN pip3 install poetry
RUN poetry config virtualenvs.create false
COPY . .
RUN poetry install --only main
CMD ["gunicorn", "-w", "1", "-k", "uvicorn.workers.UvicornWorker", "crmi_syndication.main:app", "--bind", "0.0.0.0:8000", "--access-logfile", "'-'"]