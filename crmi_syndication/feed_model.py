import json
import uuid
from http import HTTPStatus
from pathlib import Path
from typing import List, Dict, Literal, Union

import zulu
from fastapi import HTTPException
from feedgen.entry import FeedEntry
from feedgen.ext.base import BaseExtension, BaseEntryExtension
from feedgen.feed import FeedGenerator
from feedgen.util import xml_elem
from pydantic import BaseModel

from crmi_syndication.app_version import get_app_version


class JsonKeys:
    PACKAGE_LIST = "packages"
    FEED_UPDATED_AT = "updated_at"
    FEED_ID = "id"
    PACKAGE_NAME = "name"
    PACKAGE_VERSION = "version"
    PACKAGE_DOWNLOAD_TARBALL = "download_tarball"
    PACKAGE_PUBLISHED = "published"
    FHIR_VERSION = "fhir_version"


HL7_NS = "http://hl7.org/fhir/uv/crmi/syndication"


class CrmiFeedExtension(BaseExtension):
    def extend_ns(self):
        return {"hl7": HL7_NS}


class CrmiEntryExtension(BaseEntryExtension):
    __fhir_version: Union[str, None]
    __package_version: Union[str, None]

    def __init__(self):
        self.__fhir_version = None
        self.__package_version = None

    @staticmethod
    def _add_element(entry, name, value):
        elem = xml_elem(f"{{{HL7_NS}}}{name}", entry)
        elem.text = value

    def extend_atom(self, entry: FeedEntry):
        self._add_element(entry, "fhirVersion", self.__fhir_version)
        self._add_element(entry, "artifactType", "resource")
        self._add_element(entry, "publishAction", "publish")
        self._add_element(entry, "artifactVersion", self.__package_version)
        return entry

    def fhir_version(self, fhir_version: str):
        if fhir_version is not None:
            self.__fhir_version = fhir_version
        return self.__fhir_version

    def package_version(self, package_version: str):
        if package_version is not None:
            self.__package_version = package_version
        return self.__package_version


class PackageToDelete(BaseModel):
    name: str
    version: str


class PackageToRegister(BaseModel):
    name: str
    version: str
    download_tarball: str
    fhir_version: Literal["4.0.0", "4.0.1", "4.3.0", "5.0.0"]


class RegisteredPackage:
    id: str
    published: str
    package_name: str
    package_version: str
    download_tarball: str
    fhir_version: Literal["4.0.0", "4.0.1", "4.3.0", "5.0.0"]

    def __init__(
        self,
        package_name: str,
        package_version: str,
        download_tarball: str,
        fhir_version: Literal["4.0.0", "4.0.1", "4.3.0", "5.0.0"],
        published: str = zulu.now().isoformat(),
    ):
        self.published = published
        self.package_name = package_name
        self.package_version = package_version
        self.download_tarball = download_tarball
        self.fhir_version = fhir_version
        self.id = self.generate_urn_id()

    def generate_urn_id(self):
        return f"mii-termserv://{self.package_name}/{self.package_version}"

    def to_json(self):
        return {
            JsonKeys.PACKAGE_NAME: self.package_name,
            JsonKeys.PACKAGE_VERSION: self.package_version,
            JsonKeys.PACKAGE_DOWNLOAD_TARBALL: self.download_tarball,
            JsonKeys.PACKAGE_PUBLISHED: self.published,
            JsonKeys.FHIR_VERSION: self.fhir_version,
        }


class Feed:
    feed_title: str
    feed_link: str
    author_name: str
    author_email: str
    feed_id: str

    storage_path: Path
    updated_at: str

    registered_packages: List[RegisteredPackage]

    def __init__(
        self,
        feed_title: str,
        feed_link: str,
        author_name: str,
        author_email: str,
        storage_path: Path,
    ):
        self.feed_title = feed_title
        self.feed_link = feed_link
        self.author_name = author_name
        self.author_email = author_email
        self.storage_path = storage_path
        self.__read_storage_from_file()

    def __read_storage_from_file(self):
        if not self.storage_path.exists():
            print("Storage path doesn't exist, creating a new empty file")
            self.updated_at = zulu.now().isoformat()
            self.registered_packages = []
            self.feed_id = self.__generate_feed_id()
            self.__write_storage()
        with open(self.storage_path, "r") as jf:
            j: Dict = json.load(jf)
            self.updated_at = zulu.parse(j[JsonKeys.FEED_UPDATED_AT]).isoformat()
            self.feed_id = j[JsonKeys.FEED_ID]
            packages = j[JsonKeys.PACKAGE_LIST]
            self.registered_packages = self.__read_packages(packages)

    def generate_feed(self):
        fg = FeedGenerator()
        fg.register_extension(
            namespace="hl7",
            extension_class_feed=CrmiFeedExtension,
            extension_class_entry=CrmiEntryExtension,
            rss=False,
            atom=True,
        )
        fg.generator(
            generator="CRMI Syndication Tool for MII SU-TermServ",
            uri="https://gitlab.com/mii-termserv/crmi-syndication.git",
            version=get_app_version(),
        )
        fg.id(self.feed_id)
        fg.title(self.feed_title)
        fg.updated(self.updated_at)
        fg.link(href=self.feed_link, rel="self")
        fg.author(name=self.author_name, email=self.author_email)
        self.add_feed_elements(fg)
        return_str = '<?xml version="1.0" encoding="UTF-8"?>\n'
        return_str += '<?xml-stylesheet type="text/xsl" href="/crmi/static/crmi.xsl"?>\n'
        xml = fg.atom_str(pretty=True, xml_declaration=False)
        return return_str + xml.decode("utf-8")

    @staticmethod
    def __generate_feed_id():
        return f"urn:uuid:{uuid.uuid4()}"

    def __write_storage(self):
        with open(self.storage_path, "w") as f:
            j = {
                JsonKeys.FEED_UPDATED_AT: self.updated_at,
                JsonKeys.FEED_ID: self.feed_id,
                JsonKeys.PACKAGE_LIST: [p.to_json() for p in self.registered_packages],
            }
            json.dump(j, f, indent=2)

    def add_package_to_feed(self, package: PackageToRegister):
        package_to_register = RegisteredPackage(
            package_name=package.name,
            package_version=package.version,
            download_tarball=package.download_tarball,
            fhir_version=package.fhir_version
        )
        if any(p for p in self.registered_packages if p.id == package_to_register.id):
            raise HTTPException(
                status_code=HTTPStatus.CONFLICT.value,
                detail=f"The package {package_to_register.id} already exists",
            )
        self.registered_packages.append(package_to_register)
        self.updated_at = zulu.now().isoformat()
        self.__write_storage()
        return package_to_register

    @staticmethod
    def __read_packages(packages: List[Dict]) -> List[RegisteredPackage]:
        read_packages = []
        for p in packages:
            package_name = p[JsonKeys.PACKAGE_NAME]
            package_version = p[JsonKeys.PACKAGE_VERSION]
            download_tarball = p[JsonKeys.PACKAGE_DOWNLOAD_TARBALL]
            published = p[JsonKeys.PACKAGE_PUBLISHED]
            fhir_version = p[JsonKeys.FHIR_VERSION]
            package = RegisteredPackage(
                package_name=package_name,
                package_version=package_version,
                download_tarball=download_tarball,
                published=published,
                fhir_version=fhir_version,
            )
            read_packages.append(package)

        return read_packages

    def add_feed_elements(self, fg):
        for p in sorted(
            self.registered_packages,
            key=lambda pack: pack.generate_urn_id(),
            reverse=True,
        ):
            entry = fg.add_entry()
            entry.id(p.generate_urn_id())
            entry.title(p.package_name)
            entry.updated(p.published)
            entry.hl7.fhir_version(p.fhir_version)
            entry.hl7.package_version(p.package_version)
            entry.link(
                rel="alternate", type="application/fhir+gzip", href=p.download_tarball
            )

    def delete_package_from_feed(self, package: PackageToDelete):
        matching_package = next(
            (
                p
                for p in self.registered_packages
                if p.package_name == package.name
                and p.package_version == package.version
            ),
            None,
        )
        if matching_package is None:
            raise HTTPException(
                status_code=HTTPStatus.NOT_FOUND.value,
                detail=f"The package {package.name} version {package.version} isn't registered",
            )
        self.updated_at = zulu.now().isoformat()
        self.registered_packages.remove(matching_package)
        self.__write_storage()
        return {
            "deleted": True,
            "name": package.name,
            "version": package.version,
            "fhir_version": matching_package.fhir_version,
            "download_tarball": matching_package.download_tarball,
        }
