import logging
import sys
from typing import Annotated

from dotenv import load_dotenv
from fastapi import FastAPI, Response, Depends
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from fastapi.templating import Jinja2Templates
from starlette.responses import RedirectResponse
from fastapi.staticfiles import StaticFiles
import uvicorn

from crmi_syndication.app_version import get_app_version
from crmi_syndication.configuration import (
    configure_feed,
    get_env_var_or_raise,
    read_or_generate_authorization_token,
    validate_token,
)
from crmi_syndication.feed_model import PackageToRegister, PackageToDelete

load_dotenv()
app = FastAPI(
    description="This service is hosted under the path /crmi, please refer to the docs at [/crmi/docs](/crmi/docs) for more information."
)

crmi_app = FastAPI(
    title="MII SU-TermServ HL7 FHIR Canonical Resource Management Infrastructure Syndication Service",
    description="""
This project implemements a RESTful API for the Atom-based [[RFC4287]](https://datatracker.ietf.org/doc/html/rfc4287) Syndication Feed profiled in the Distribution section of the FHIR Canonical Resources Management IG [[CRMI](https://hl7.org/fhir/uv/crmi/distribution.html#distribution-syndication)].

The API is implemented using the [FastAPI](https://fastapi.tiangolo.com/) framework. Write requests require authentication using random key that is generated and written to disk on the first application launch.
""",
    version=get_app_version(),
)
crmi_app.mount("/static", StaticFiles(directory="static"), name="static")
app.mount("/crmi", crmi_app)
feed = configure_feed()
security_scheme = HTTPBearer(
    description="The bearer token generated at the first start of the application"
)
expected_token = read_or_generate_authorization_token()
logger = logging.getLogger(__name__)
logger.setLevel(get_env_var_or_raise("LOG_LEVEL", "the log level", default="INFO"))
stream_handler = logging.StreamHandler(sys.stdout)
log_formatter = logging.Formatter(
    "%(asctime)s [%(levelname)s] %(name)s: %(message)s"
)
stream_handler.setFormatter(log_formatter)
logger.addHandler(stream_handler)
logger.info(f"Started CRMI API version {get_app_version()}")


def start():
    print(start)
    port = get_env_var_or_raise(
        "PORT", "the port to run the application on", default="8000"
    )
    bind_host = get_env_var_or_raise(
        "BIND_HOST", "the host to bind the application to", default="0.0.0.0"
    )
    print(port, bind_host)
    uvicorn.run(app, host=bind_host, port=int(port), workers=1)


@crmi_app.get("/syndication.xml")
async def get_feed():
    logger.debug("get_feed")
    return Response(content=feed.generate_feed(), media_type="application/xml")


@crmi_app.put("/add")
async def add_feed_item(
    package: PackageToRegister,
    received_token: Annotated[HTTPAuthorizationCredentials, Depends(security_scheme)],
):
    logger.debug(f"add_feed_item {package}")
    validate_token(received_token, expected_token)
    return feed.add_package_to_feed(package).to_json()


@crmi_app.delete("/delete")
async def delete_feed_item(
    package: PackageToDelete,
    received_token: Annotated[HTTPAuthorizationCredentials, Depends(security_scheme)],
):
    logger.debug(f"delete_feed_item {package}")
    validate_token(received_token, expected_token)
    return feed.delete_package_from_feed(package)


@app.get("/favicon.png", include_in_schema=False)
async def favicon_png():
    logger.debug("favicon.png")
    return RedirectResponse("/crmi/static/favicon.png", status_code=308)


@app.get("/favicon.ico", include_in_schema=False)
async def favicon_ico():
    logger.debug("favicon.ico")
    return RedirectResponse("/crmi/static/favicon.ico", status_code=308)


@app.get("/", include_in_schema=True)
async def redirected():
    logger.debug("redirect /")
    return RedirectResponse("/crmi/syndication.xml", status_code=308)


@crmi_app.get("/", include_in_schema=False)
async def redirected_crmi():
    logger.debug("redirect /crmi")
    return RedirectResponse("/crmi/syndication.xml", status_code=308)


if __name__ == "__main__":
    start()
