import crmi_syndication


def get_app_version():
    return crmi_syndication.version.rstrip("+")
