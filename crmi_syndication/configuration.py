import os
from pathlib import Path
import secrets
from typing import Union

from fastapi import HTTPException
from fastapi.security import HTTPAuthorizationCredentials

from crmi_syndication.feed_model import Feed


def get_env_var_or_raise(
    var_name: str, error_msg: Union[str, None], default=None
) -> str:
    var_value = os.getenv(var_name, default)
    if var_value is None:
        raise ValueError(f"Environment variable {var_name} not set - {error_msg}")
    return var_value


def configure_feed():
    feed_title = get_env_var_or_raise("FEED_TITLE", "the <title> element of the feed")
    self_link = get_env_var_or_raise(
        "SELF_LINK",
        "The <link> element of the feed, i.e. the location this feed is available from",
    )
    author_name = get_env_var_or_raise(
        "AUTHOR_NAME", "The name in the <author> element of the feed"
    )
    author_email = get_env_var_or_raise(
        "AUTHOR_EMAIL", "The e-mail in the <author> element of the feed"
    )
    storage_location = Path(
        get_env_var_or_raise(
            "STORAGE_LOCATION", "the location the app storage (JSON file) is written to"
        )
    )
    return Feed(feed_title, self_link, author_name, author_email, storage_location)


def read_or_generate_authorization_token():
    token_file = Path(
        get_env_var_or_raise(
            "TOKEN_FILE_PATH",
            "the path to read the authorization token from (and to store it once generated)",
        )
    )
    if not token_file.exists():
        with open(token_file, "w") as f:
            new_token = secrets.token_urlsafe(32)
            f.write(new_token)
            print(f"Generated new access token: {new_token}")
            print(f"Wrote access token to {token_file.absolute()}")
            return new_token
    with open(token_file, "r") as f:
        print(f"Using access token stored in {token_file.absolute()}")
        return f.read().strip()


def validate_token(received_token: HTTPAuthorizationCredentials, expected_token: str):
    if received_token.credentials.strip() != expected_token:
        raise HTTPException(401, detail="The token you provided is invalid")
