from usingversion import getattr_with_version

__getattr__ = getattr_with_version("crmi_syndication", __file__, __name__)
