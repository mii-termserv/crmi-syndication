<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:hl7="http://hl7.org/fhir/uv/crmi/syndication"
    xmlns:atom="http://www.w3.org/2005/Atom"
    xmlns:xhtml="http://www.w3.org/1999/xhtml">
    
    <xsl:output method="html" indent="yes" encoding="UTF-8"/>
    
    <xsl:template match="/">
        <html>
            <head>
                <title>
                    CRMI Syndication Feed
                </title>
                <link rel="stylesheet" href="/crmi/static/tachyons.min.css"/>
                <link rel="stylesheet" href="/crmi/static/style.css"/>
            </head>
            <body class="ph1 pb3 mid-gray">
                <header class="mw9 ph4 pv4 center">
                    <div class="flex items-center">
                        <h1 class="ma0 mr2 f2 mii-green"><xsl:value-of select="atom:feed/atom:title"/></h1>
                    </div>
                    
                    <h2 class="ma0 mt4 f4 normal">
                        This feed contains 
                        <strong class="mii-blue"><xsl:value-of select="count(atom:feed/atom:entry)"/></strong>
                        packages.
                    </h2>
                    
                    <div class="ma0 mt2 f4 normal">
                        <table class="w-100" >
                            <tbody>
                                <tr>
                                    <th>Last updated</th>
                                    <td><xsl:value-of select="atom:feed/atom:updated"/></td>
                                </tr>
                                <tr>
                                    <th>ID</th>
                                    <td><xsl:value-of select="atom:feed/atom:id"/></td>
                                </tr>
                                <tr>
                                    <th>Author</th>
                                    <td><xsl:value-of select="atom:feed/atom:author/atom:name"/> &lt;<xsl:value-of select="atom:feed/atom:author/atom:email"/>&gt;</td>
                                </tr>
                                <tr>
                                    <th>Generator</th>
                                    <td><xsl:value-of select="atom:feed/atom:generator"/> (Version: <xsl:value-of select="atom:feed/atom:generator/@version"/>)</td>
                                </tr>
                                <tr>
                                    <th>Link</th>
                                    <td><a href="{atom:feed/atom:link/@href}"><xsl:value-of select="atom:feed/atom:link/@href"/></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <p>
                        This is an Atom syndication feed as specified in the HL7 Canonical Resources Management Infrastructure Implementation Guide <a href="https://hl7.org/fhir/uv/crmi/distribution.html#distribution-syndication">[CRMI]</a>.<br/>
                        It is generated using an open-source API, available from <a href="https://gitlab.com/mii-termserv/crmi-syndication">GitLab</a> and licensed under CC-BY-4.0.<br/>
                        This document is an XML file that can be read using FHIR-CRMI-compliant tooling, this presentation is generated using a stylesheet by your browser. Download the raw XML here: 
                        <a class="f6 link dim ba ph3 pv2 mb2 dib mii-blue" href="{atom:feed/atom:link/@href}" download="syndication.xml">Download this XML file</a>
                    </p>
                </header>
                
                <xsl:apply-templates />
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="atom:feed">
        <div class="mw9 ph4 center">
            <div class="overflow-auto">
                <table class="w-100 f6 b--silver ba bw1" cellspacing="0">
                    <thead class="bg-mii-blue">
                        <tr>
                            <th class="pa3 fw6 tl white" style="width:60px"/>
                            <th class="pa3 fw6 tl white">ID/Link</th>
                            <th class="pa3 fw6 tl white">Title</th>
                            <th class="pa3 fw6 tl white">Version<br/><span class="mii-green f7 ba ph1" title="alpha=magenta, beta=violet, rc=blue, stable=black, else=grey">color key?</span></th>
                            <th class="pa3 fw6 tl white">FHIR Version<br/><span class="mii-green f7 ba ph1" title="4.0.0=R4=magenta, 4.0.1=R4+TC=blue, 4.3.0=R4B=yellow, 5.0.0=R5=orange, else=grey">color key?</span></th>
                            <th class="pa3 fw6 tr white">Updated</th>
                        </tr>
                    </thead>
                    <tbody class="lh-copy bg-near-white">
                        <xsl:choose>
                            <xsl:when test="atom:entry">
                                <xsl:apply-templates select="atom:entry"> 
                                    <xsl:sort select="atom:title"/>
                                </xsl:apply-templates>    
                            </xsl:when>
                            <xsl:otherwise>
                                <tr>
                                    <td class="pa3 tc b bb b--silver" colspan="6">No entries in feed</td>
                                </tr>
                            </xsl:otherwise>
                        </xsl:choose>
                    </tbody>
                </table>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="atom:entry">
        <tr class="hover-bg-white">
            <xsl:variable name="pno">
                <xsl:value-of select="position()"/>
            </xsl:variable>
            <td class="pa3 tc b bb b--silver">
                <xsl:value-of select="$pno"/>
            </td>
            <td class="pa3 b bb b--silver break-word">
                <a href="{atom:link/@href}">
                    <xsl:value-of select="atom:id"/>
                </a>
            </td>
            <td class="pa3 b bb b--silver break-word">
                <xsl:value-of select="atom:title"/>
            </td>
            <td class="pa3 b bb b--silver break-word">
                <xsl:variable name="av" select="hl7:artifactVersion"/>
                <xsl:variable name="pill_color">
                    <xsl:choose>
                        <xsl:when test="contains($av, 'alpha')">
                            bg-ibm-3
                        </xsl:when>
                        <xsl:when test="contains($av, 'beta')">
                            bg-ibm-2
                        </xsl:when>
                        <xsl:when test="contains($av, 'rc')">
                            bg-ibm-1
                        </xsl:when>
                        <xsl:when test="not(contains($av, '-'))">
                            bg-black
                        </xsl:when>
                        <xsl:otherwise>
                            bg-silver
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <div class="dib mr2 ph2 pv1 tracked lh-solid white br-pill break-word {$pill_color}">
                    <xsl:value-of select="hl7:artifactVersion"/>
                </div>
            </td>
            <td class="pa3 b bb b--silver">
                <xsl:variable name="pill_color">
                    <xsl:choose>
                        <xsl:when test="hl7:fhirVersion = '4.0.0'">
                            bg-ibm-3
                        </xsl:when>
                        <xsl:when test="hl7:fhirVersion = '4.0.1'">
                            bg-ibm-1
                        </xsl:when>
                        <xsl:when test="hl7:fhirVersion = '4.3.0'">
                            bg-ibm-5
                        </xsl:when>
                        <xsl:when test="hl7:fhirVersion = '5.0.0'">
                            bg-ibm-4
                        </xsl:when>
                        <xsl:otherwise>
                            bg-silver
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <div class="dib mr2 ph2 pv1 tracked lh-solid white br-pill break-word {$pill_color}">
                    FHIR <xsl:value-of select="hl7:fhirVersion"/>
                </div>
            </td>
            <td class="pa3 b bb b--silver break-word">
                <xsl:value-of select="atom:updated"/>
            </td>
        </tr>
    </xsl:template>
    
</xsl:stylesheet>